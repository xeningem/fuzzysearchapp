﻿using System;
using System.Linq;
using FuzzySearch.Lib;

namespace FuzzySearch.App
{
    class RunWithOption
    {
        public static int RunPairAndReturnExitCode(PairVerb verb)
        {
            var values = verb.Values.ToList();
            Console.WriteLine(LevenshteinDistance.Distance(values[0], values[1]));
            return 0;
        }

        public static int RunPromptAndReturnExitCode(PromptVerb verb)
        {
            while (true)
            {
                Console.Write("Enter first string: ");
                var firstLine = Console.ReadLine();

                Console.Write("Enter second string: ");
                var seconLine = Console.ReadLine();

                Console.WriteLine(LevenshteinDistance.Distance(firstLine, seconLine));
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}
