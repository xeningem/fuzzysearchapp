﻿using System.Collections.Generic;
using CommandLine;

namespace FuzzySearch.App
{
    [Verb("pair", HelpText = "Show editor distance for two string.")]
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class PairVerb {
        [Value(0, Min = 2, Max = 2)]
        public IList<string> Values { get; set; }
    }
}