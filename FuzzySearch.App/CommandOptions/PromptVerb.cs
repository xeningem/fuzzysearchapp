﻿using System.Collections.Generic;
using CommandLine;

namespace FuzzySearch.App
{
    [Verb("prompt", HelpText = "Compare phrase via command prompt.")]
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class PromptVerb
    {
    }
}