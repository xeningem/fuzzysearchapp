﻿using CommandLine;

namespace FuzzySearch.App
{
    static class Program
    {
        static int Main(string[] args)
        {
            var result = Parser.Default.ParseArguments<PairVerb, PromptVerb>(args);
            
            if (result.Tag == ParserResultType.Parsed)
            {
                return result.MapResult(
                    (PairVerb verb) => RunWithOption.RunPairAndReturnExitCode(verb),
                    (PromptVerb verb) => RunWithOption.RunPromptAndReturnExitCode(verb),
                    _ => 1);
            }

            return 0;
        }
    }
}
