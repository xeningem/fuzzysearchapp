﻿using System;

namespace FuzzySearch.Lib
{
    public class LevenshteinDistance
    {
       
        public static int Distance(string s1, string s2, bool useDebugPrint = false)
        {
            if (string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2))
            {
                var s1Length = s1?.Length;
                var s2Length = s2?.Length;
                if (s1Length.HasValue && s1Length.Value > 0)
                {
                    return s1Length.Value;
                }

                if (s2Length.HasValue && s2Length.Value > 0)
                {
                    return s2Length.Value;
                }

                return 0;
            }

            // Using standart notation for Levenshtein distance from Wikipedia
            var m = s1.Length;
            var n = s2.Length;

            var showLog = useDebugPrint && n < 20 && m < 20;
            var costs = new int[n];

            if (showLog)
            {
                Console.WriteLine(s1 + " " + s2);
            }
            // Add indexing for insertion to first row
            for (var i = 0; i < n;)
            {
                costs[i] = ++i;
            }
           
            for (var i = 0; i < m; i++)
            {
                if (showLog)
                {
                    Console.WriteLine(string.Join(" ", costs));
                }
                // cost of the first index
                int cost = i;
                int addationCost = i;

                // cache value for inner loop to avoid index lookup and bonds checking, profiled this is quicker
                char value1Char = s1[i];

                for (int j = 0; j < n; j++)
                {
                    var insertionCost = cost;

                    cost = addationCost;

                    // assigning this here reduces the array reads we do, improvment of the old version
                    addationCost = costs[j];

                    if (value1Char != s2[j])
                    {
                        if (insertionCost < cost)
                        {
                            cost = insertionCost;
                        }

                        if (addationCost < cost)
                        {
                            cost = addationCost;
                        }

                        cost++;
                    }

                    
                    costs[j] = cost;
                }

            }
            if (showLog)
            {
                Console.WriteLine(string.Join(" ", costs));
            }
            return costs[costs.Length - 1];
        }
    }
}