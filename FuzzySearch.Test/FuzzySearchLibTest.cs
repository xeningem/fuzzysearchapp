﻿using System;
using System.Collections.Generic;
using System.Linq;
using FuzzySearch.Lib;
using NUnit.Framework;

namespace FuzzySearch.Test
{
    [TestFixture]
    public class FuzzySearchLibTest
    {
        private class CompareResult
        {
            public readonly string S1;
            public readonly string S2;
            public readonly int Distance;
            public readonly string Description;

            public CompareResult(string s1, string s2, int distance, string description = null)
            {
                S1 = s1;
                S2 = s2;
                Distance = distance;
                Description = description;
            }
        }

        private void TestLevenshtein(Func<string, string, bool, int> func, List<CompareResult> distanceInfo)
        {
            foreach (var item in distanceInfo)
            {
                var result = func(item.S1, item.S2, true);
                if (item.Distance < 0)
                {
                    continue;
                }
                Assert.AreEqual(item.Distance, result, $"{item.Description??""} S1: {item.S1} S2: {item.S2}");
            }
        }

        [Test]
        public void TestLevenshteinCommonString()
        {
            const int repeatNumer = 500;
            var longString1 = string.Concat(Enumerable.Repeat(
                "First, when developing testing programs you will often want to loop through user inputs.", repeatNumer));
            var longString2 = longString1.Replace("loop", "pool");

            var distanceInfo = new List<CompareResult>()
            {
                new CompareResult(null, "", 0, "Empty string and null"),
                new CompareResult("", null, 0, "Empty string and null"),
                new CompareResult("", "", 0, "Empty string"),
                new CompareResult("", "empty", 5, "Compare with empty string"),
                new CompareResult("empty", "empty", 0, "Equal string"),
                new CompareResult("ant", "aunt", 1),
                new CompareResult("aunt", "ant", 1, "Should be equal for different parameters order"),
                new CompareResult("Sam", "Samantha", 5),
                new CompareResult("clozapine", "olanzapine", 3),
                new CompareResult("flomax", "volmax", 3),
                new CompareResult("toradol", "tramadol", 3),
                new CompareResult("kitten", "sitting", 3),
                new CompareResult("loop", "pool", 2),
                new CompareResult("blmost the same", "almost the samw", 2),
                new CompareResult(longString1, longString2, 2 * repeatNumer, "Should work with long string"),
                new CompareResult(longString2, longString1, 2 * repeatNumer),
                new CompareResult("loop", longString2, -1),
                new CompareResult(longString2, "loop",  -1)
            };

            TestLevenshtein(LevenshteinDistance.Distance, distanceInfo);
        }

        [Test]
        public void TestLevenshteinWithNullAndEmptyString()
        {
            var distanceInfo = new List<CompareResult>()
            {
                new CompareResult(null, "", 0, "Empty string and null"),
                new CompareResult("", null, 0, "Empty string and null"),
            };

            TestLevenshtein(LevenshteinDistance.Distance, distanceInfo);
        }

        [Test]
        public void TestLevenshteinWithLongString()
        {
            const int repeatNumer = 500;
            var longString1 = string.Concat(Enumerable.Repeat(
                "First, when developing testing programs you will often want to loop through user inputs.", repeatNumer));
            var longString2 = longString1.Replace("loop", "pool");

            var distanceInfo = new List<CompareResult>()
            {
                new CompareResult(longString1, longString2, 2 * repeatNumer, "Should work with long string"),
                new CompareResult(longString2, longString1, 2 * repeatNumer, "Should work with long string"),
                new CompareResult("loop", longString2, -1),
                new CompareResult(longString2, "loop",  -1)
            };

            TestLevenshtein(LevenshteinDistance.Distance, distanceInfo);
        }
    }
}
